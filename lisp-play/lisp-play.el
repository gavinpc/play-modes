;;; -*- lexical-binding: t -*-

;; TODO: you always get "end of file during parsing" after commenting something.
;; Making any other edit restores it though.

;; a really quick-and-dirty "play" mode for lisp
;; essentially stolen from js-play

(setq lisp-play--output-buffer nil)

(defvar lisp-play-imperative-mode nil
  "If non-nil, evaluate expressions from within output buffer
after clearing it.  This allows you to use side-effects on the
buffer itself.")

(defvar lisp-play--output-buffer nil
  "The buffer dedicated to the results of the input source.")

(defvar lisp-play--previous-buffer nil)

(defvar lisp-play-input-buffer nil
  "The lisp buffer that's wired to the live output.")

(defun lisp-play--ensure-buffer-visible (buffer)
  "Display the output window if it is not already visible."
  (unless (get-buffer-window buffer)
	(display-buffer buffer)))

(defun lisp-play--ensure-output-visible ()
  (lisp-play--ensure-buffer-visible (lisp-play-output-buffer)))

(defun lisp-play--window-config-changed ()
  "Ensure that output buffer is shown whenever input buffer is
  selected, and vice versa."
  (let ((prev lisp-play--previous-buffer)
		(current (current-buffer))
		(input lisp-play-input-buffer)
		(output (lisp-play-output-buffer)))
	(unless (eq current prev)
	  (setq lisp-play--previous-buffer (current-buffer))
	  (cond
	   ((eq current input) (lisp-play--ensure-buffer-visible output))
	   ((eq current output) (lisp-play--ensure-buffer-visible input))))))

(defun safe-eval (code)
  "Evaluate Emacs Lisp CODE with error trapping.  You still need
to abort using C-g if the code enters an infinite loop."
  (let ((debug-on-exit t))
	(condition-case err
		(eval (read (concat "(progn " code " )")) t)
	  (error (error-message-string err)))))

(defun lisp-play--show-results (code)
  "Evaluate CODE in and replace the contents of
  `lisp-play-output-buffer' with the results."
  (let* ((buffer (lisp-play-output-buffer))
		(imperative lisp-play-imperative-mode)
		;; Still requires you to press C-g if you're in an infinite loop.
		(result (unless imperative (safe-eval code))))
	(with-current-buffer buffer
	  (erase-buffer)
	  (if imperative (setq result (safe-eval code)))
	  (print result buffer)
	  (lisp-play--ensure-buffer-visible buffer)
	  (goto-char (point-min)))))

(defun lisp-play--cleanup-output ()
  "Remove hooks used by the output buffer."
  (remove-hook 'buffer-list-update-hook 'lisp-play--window-config-changed)
  (if lisp-play-input-buffer
	  (with-current-buffer lisp-play-input-buffer
		(remove-hook 'after-change-functions 'lisp-play--after-change t)
		(remove-hook 'after-change-functions 'debug--after-change t))))

(defun lisp-play--after-change (change-beg change-end prev-len)
  "Respond to buffer changes by triggering output update"
  (lisp-play--send-source))

(defun lisp-play--cleanup-everything ()
  "Remove everything associated with the watcher."
  (lisp-play--cleanup-output)
  (kill-buffer lisp-play--output-buffer))

(defun lisp-play--send-source ()
  "Submit contents of the input buffer for lisp evaluation.
This should be run whenever you've made a change to the source.
Note that this proceeds even if the buffer has parse errors."
  (lisp-play--show-results (buffer-string)))

(defun lisp-play--start-with-buffer (buffer)
  "Configure BUFFER to serve as input source for real-time lisp
evaluation."
  (with-current-buffer buffer
	(setq lisp-play-input-buffer buffer)
	
	(add-hook 'kill-buffer-hook 'lisp-play--cleanup-everything t t)
	(add-hook 'buffer-list-update-hook 'lisp-play--window-config-changed)
	
	(add-hook 'after-change-functions 'lisp-play--after-change nil t)
	(lisp-play--send-source)))

(defun lisp-play-output-buffer ()
  "Return the output buffer, creating it if necessary."
  (or
   (and (buffer-live-p lisp-play--output-buffer)
		lisp-play--output-buffer)
   (setq lisp-play--output-buffer (get-buffer-create "*lisp play*"))))

(defun lisp-play-start ()
  "Bind the current lisp buffer to a dedicated output buffer."
  (interactive)
  (lisp-play--start-with-buffer (current-buffer)))

(provide 'lisp-play)
