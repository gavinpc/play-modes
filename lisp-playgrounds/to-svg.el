;; EVAL THIS FIRST!
;; (setq lisp-play-imperative-mode t)

(require 'lisp-to-svg)

(setq special-mappings
	  '(
		((lambda (o contexts)
		   (> (length contexts) 3))
		 (lambda (o contexts)
		   (string-to-svg "deep!" contexts)
		   )
		 t
		 )

		((lambda (o)
		   (eq o 'head)
		   )
		 (lambda (o &optional contexts)
		   (string-to-svg "You're not AND!" contexts)
		   )
		 )
		))


;; Here's some code that draws a box for the current context.

(if nil
	(let* ((parent-context (first contexts)))
	  (destructuring-bind
		  (&whole w &key (width 100) (height 100) &allow-other-keys)
		  parent-context
		(if (cadr contexts)				; i.e. not first level
			`(rect (:width ,width :height ,height
						   :fill "rgba(200, 100, 100, .3)"
						   :stroke "yellow"
						   :stroke "#333"
						   :stroke-width 8))))))

(let* ((window (get-buffer-window lisp-play--output-buffer))
	   (width (float (window-pixel-width)))
	   (extra-height (cdr (window-text-pixel-size window nil nil nil nil t)))
	   (height (- (float (window-pixel-height window)) extra-height))
	   (initial-context (list :width width :height height))
	   (example-tree `[lambda [x y]
						[append x [cons x y]]
						])
	   (example-tree `(once upon a time and a very good time it was))
	   (example-tree
		'[body [head neck] torso
			   [[left-arm [elbow wrist [hand 1 2 3 4 5]]]
					  [right-arm [elbow wrist [hand 1 2 3 4 5]]]]
			   [[left-leg [knee ankle [foot 1 2 3 4 5]]]
				[right-leg [knee ankle [foot 1 2 3 4 5]]]]
			   ])
	   (lisp-to-svg-mappings (append special-mappings lisp-to-svg-mappings))
	   (object-svg
		(lisp-to-svg example-tree
					 (list initial-context)))
	   )
  object-svg
  (svg
   (lisp-to-xml
   	`(svg (:width ,(* width 1) :height ,(* height 1))
		  (rect (:x 0 :y 0 :width "100%" :height "100%" :fill "none" :stroke "yellow" :stroke-width 10))
   		  ,@object-svg
   		  )))
  )
