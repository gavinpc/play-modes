;; Set this before evaluating
;; (setq lisp-play-imperative-mode t)

(defun isoceles (h x y)
  "Return a path object for an upward-pointing isoceles triangle
of height H with top at (X, Y)."
  (let ((a2 (/ h (sqrt 3))))
	`(path (:d ,(format "m %s,%s l %s,%s l %s,0 z" x y a2 h  (* a2 -2.0)) :stroke "blue" :fill "red" :stroke-width 0))))

(defun sier (height depth level at)
  (let ((x (car at)) (y (cdr at)))
	(if (eq depth level)
		(isoceles height x y)
	  (let* ((h2 (/ height 2.0))
			 (a4 (/ height (sqrt 3) 2))
			 (tris (map 'list
						(apply-partially 'sier h2 depth (1+ level))
						(list (cons x y)
							  (cons (+ x a4) (+ y h2))
							  (cons (- x a4) (+ y h2))))))
		(if (eq level (1- depth)) tris (apply #'append tris))))))


(let* ((window (get-buffer-window lisp-play--output-buffer))
	   (width (float (window-pixel-width)))
	   (extra-height (cdr (window-text-pixel-size window nil nil nil nil t)))
	   (height (- (float (window-pixel-height window)) extra-height))
	   (h 100.0)
	   (sqrt2 (sqrt 2))
	   (a2 (* h sqrt2)))
  (svg
   (lisp-to-xml
	`(svg (:width ,width :height ,height)
		  (rect (:x 0 :y 0 :width "100%" :height "100%" :fill "white"))
		  ,@(sier 800 5 0 (cons (/ width 2.0) 0))
		  ;;,(isoceles 800 (/ width 2.0) 0)
		  ))))

;;(sier 500 2 0 '(1000 . 200))
