;; alternate approach to Sierpinksi based on re-use and transform
(let* ((window (get-buffer-window lisp-play--output-buffer))
	   (width (float (window-pixel-width)))
	   (extra-height (cdr (window-text-pixel-size window nil nil nil nil t)))
	   (height (- (float (window-pixel-height window)) extra-height)))
  (svg
   (lisp-to-xml
	`(svg (:width ,(* width 1) :height ,(* height 1)) ; seems to work without :xmlns:xlink "http://www.w3.org/1999/xlink"
		  (defs ()
			,@(loop for n from 1 upto 10 with next
					do (setq next (format "#sier%s" (1- n)))
					collect
					;; (/ 2.0 (sqrt 3))	 ; 1.1547005383792517
					;; (/ 1.0 (sqrt 3))	 ; 0.5773502691896258
					;; WHY does this viewbox work?
					`(svg (:id ,(format "sier%d" n) :viewBox "-57.73 0 157.73 100")
						  (rect (:x 0 :y 0 :width "100%" :height: "100%" :stroke "black" :stroke-width 1 :fill "green"))
						  (use (:xlink:href ,next :transform "scale(.5) translate(0, 0)"))
						  (use (:xlink:href ,next :transform "scale(.5) translate(-57.73, 100)"))
						  (use (:xlink:href ,next :transform "scale(.5) translate(57.73, 100)"))))
			(g (:id "sier0")
			   (path (:d "m 0,0 l 57.73,100 l -115.47,0 z" :fill "#C00"))))
		  (rect (:x 0 :y 0 :width "100%" :height "100%" :fill "white"))
		  (use (:xlink:href "#sier6"))
		  ))))
