;; A simple grammar for arithmetic expressions.  This is the example grammar
;; used in Earley's 1960 CACM paper, with the addition of the rule for
;; parenthesized expressions.
(setq plus-times-grammar
	  `[
		(E  T)
		;;(E  "(" E ")")
		(E  "{" E "}")					; easier to read in escaped output
		(T  T "*" P)
		(T  T "*" float)
		(T  P)
		(T  T "+" E)
		(P  int)
		])

(setq simple-english
	  '(
		(S VP)						; imperative
		(S NP VP)
		(S NP VP OC)					; object complement
		(OC Adjective)
		(OC NP)
		(VP Verb)
		(VP Verb DO)
		(DO NP)
		(NP Noun)
		(NP Article Noun)
		(NP Noun PP)
		(PP Prep Nominal)
		(Nominal Noun)
		(Article "the")
		(Article "an")
		(Noun "boy")
		(Noun "girl")
		(Noun "love")
		(Verb "is")
		(Verb "saw")
		(Verb "loves")
		))

;; General stuff for processing CFG's

(defun lowercase-p (char)
  "Return non-nil if CHAR is a lowercase letter."
  (<= ?a char ?z))

(defun print-term (term)
  (cond
   ((stringp term) (concat "“" term "”"))
   ((symbolp term) (symbol-name term))))


(defun is-terminal (term)
  "Tell whether the given term is a terminal.  Literal strings
are always terminals.  For symbols, the grammar form used here
assumes that (only) non-terminals are capitalized."
  (cond
   ((stringp term) t)
   ;;((symbolp term) (<= ?a (aref (symbol-name term) 0) ?z ))
   ((symbolp term) (lowercase-p (aref (symbol-name term) 0)))
   (t "error: term must be a symbol or a string")))


;; For Chomsky-normalizing grammar

;; 1. Dealing with terminals in non-normal rules

(defun rev-assoc (key seq)
  "Return the `car' of the first element of SEQ whose `cadr' is
`equal' to KEY, or `nil' if none matches.  In other words, treat
SEQ as an alist with entries in the form '(VALUE KEY ...)."
  (car (rassoc* key seq :test 'equal :key 'car)))

(defun translate-list (list replacements &optional lookup)
  "Return a copy of LIST with the elements replaced according to
the alist REPLACEMENTS.  Substitutions are determined using the
function LOOKUP, which is `assoc' by default.

This is like

    (cl-sublis REPLACEMENTS LIST :key 'cadr :test 'equal)

except that (1) it's shallow (i.e. doesn't treat LIST as a tree) and
(2) elements with no replacement are carried over as-is."
  (let ((lookup (or lookup 'assoc)))
	(map 'list
		 (lambda (x)
		   (let ((substitute (funcall lookup x replacements)))
			 (or substitute x)))
		 list)))

(defun substitute-unit-rules (rule unit-rules)
  "Return an equivalent version of RULE where any terms produced
by UNIT-RULES are substituted.  In other words, if UNIT-RULES
contains a production A -> B, then all occurrences of B are
replaced by A."
  (cons (car rule) (translate-list (cdr rule) unit-rules 'rev-assoc)))

(defun mixed-use-terminals (rule)
  "Return the non-normalized terminals in RULE."
  (let* ((rhs (cdr rule)))
	(if (cdr rhs)
		(loop for x in rhs when (is-terminal x) collect x))))


;; 2. Dealing with unit productions

(defun split-by (pred seq)
  "Separate the elements of SEQ into those for which PRED is
non-nil, and all the rest.  Return a two-element list containing
these lists, respectively."
  (loop for x being the elements of seq
		when (funcall pred x) collect x into a
		else collect x into b
		finally return (list a b)))

(defun is-unit-production (rule)
  "Return non-nil if RULE produces a single non-terminal."
  (let ((rhs (cdr rule)))
	(not (or (cdr rhs) (is-terminal (first rhs))))))

(defun expand-unit-productions (rules unit-productions)
  "Return a list of new rules resulting from the expansion of the
given UNIT-PRODUCTIONS against RULES.  That is, for a production
in the form A -> B, a set of rules {A -> y1, A -> y2} will be
returned for every B -> yn appearing in RULES.  Note that the
resulting rules may be fewer than the given unit-productions,
since not all productions will have expansions."
  (loop for (unit-lhs unit-rhs) being the elements of unit-productions
		append
		(loop for (lhs . rhs) being the elements of rules
			  when (eq lhs unit-rhs)
			  collect (cons unit-lhs rhs))))

(defun flatten-unit-productions (grammar &optional working-grammar)
  "Return a grammar equivalent to GRAMMAR, but without unit
productions.  WORKING-GRAMMAR accumulates the 'normal' (non-unit)
rules during recursion."
  (destructuring-bind
	  (unit-productions normals) (split-by 'is-unit-production grammar)
	(if working-grammar (nconc working-grammar normals)
	  (setq working-grammar normals))
	(let ((expanded (expand-unit-productions working-grammar unit-productions)))
	  (if expanded
		  (flatten-unit-productions expanded working-grammar)
		working-grammar))))


;; 3. Dealing with "long" rules (more than two terms)

(defun combine-terms (a b)
  "Return a symbol whose name is based on a combination of A's
 and B's symbol names."
  ;;(make-symbol (concat "《"  (print-term a) "●" (print-term b) "》"))
  ;;(make-symbol (concat "<"  (print-term a) "●" (print-term b) ">"))
  (make-symbol (concat  (print-term a) "●" (print-term b)))
  )

(defun binarize (rule)
  "Return a list of binary rules equivalent to RULE.  A rule in
the form A -> B C y is converted to two rules: BC -> B C, where
BC is a new term, and A -> BC y.  The latter rule is binarized
until 'y' is less than two terms."
  (destructuring-bind (lhs first &optional second &rest rest) rule
	(cond
	 (rest
	  (let ((combined (combine-terms first second)))
		`(,(list combined first second)
		  ,@(binarize `(,lhs ,combined ,@rest)))))
	 (t rule))))

(defun cnf-steps (grammar)
  (let* ((terminals-to-promote
		  (loop for rule being the elements of grammar
				append (mixed-use-terminals rule)))
		 (terminal-rules
		  (loop for term in terminals-to-promote
				collect (list (make-symbol (concat "《" (print-term term) "》")) term)))
		 (substituted-rules (loop
							 for rule being the elements of grammar
							 collect (substitute-unit-rules rule terminal-rules)))
		 (split-by-unit-production (split-by 'is-unit-production substituted-rules))
		 (flattened (flatten-unit-productions substituted-rules))
		 (binarized  (map 'vector 'binarize flattened))
		 )
	(list
	 ;;"indexed grammar" indexed-grammar "\n"
	 :grammar grammar
	 :terminals-to-promote terminals-to-promote
	 :terminal-rules terminal-rules
	 :substituted-rules substituted-rules
	 :split-by-unit-production split-by-unit-production
	 :flattened flattened
	 :hmm-flattened (flatten-unit-productions grammar)
	 :normalized binarized
	 ))
  )

(defun to-cnf (grammar)
  "Given an arbitrary context-free grammar, return a grammar in
  Chomsky Normal Form."
  )

(defun view-plist (plist)
  "Utility for printing plists, where a carriage return is
interpolated before each key-value pair."
  (loop for (key value) on plist by 'cddr
		collect key
		collect value
		collect "\n"))

(defun cyk-match (grammar left-set right-set)
  "Return a set of non-terminals A from such that A -> B C
belongs to GRAMMAR, B belongs to LEFT-SET and C belongs to
RIGHT-SET.  Assumes GRAMMAR is normalized (CNF)."
  (let ((pairs (loop for B in left-set
					 append (loop for C in right-set
								  collect (cons B C)))))
	(loop for (A B C) in grammar
		  when (find (cons B C) pairs :test 'equal)
		  collect A)))

;(unload-feature lisp-to-svg)
(require 'lisp-to-svg)

(setq cky-svg-mappings
	  '(
		((lambda (x contexts)
		   (if (= 3 (length contexts))
			   (let* ((parent (car contexts))
					  (grandparent (cadr contexts))
					  (row (plist-get parent :index))
					  (col (plist-get grandparent :index)))
				 (> row col))))
		 
		 (lambda (o contexts)
		   `(rect (:x 0 :y 0 :width "100%" :height "100%" :fill "black")))
		 t)

		((lambda (o contexts) (= 3 (length contexts)))
		 (lambda (cell contexts)
		   (lisp-to-svg `[,cell] (cons `(:direction across ,@(car contexts)) contexts))
		   ;;(string-to-svg (format "%s" cell) contexts)
		   )
		 t)
		
		))

(defun cky-recognize (grammar tokens)
  "Using the CKY algorithm, return non-nil if the sequence of
TOKENS is recognized as a sentence in the given context-free
GRAMMAR."
  ;; Initialize a vector of vectors.  This is part of the parser's state, which
  ;; I'll eventually extract.
  (let* ((len (length tokens))
		 (size (1+ len))
  		 (table (make-vector size nil)))
  	(dotimes (j size) (aset table j (make-vector size nil)))
  	(dotimes (j len table)
  	  (let ((col (aref table j)))
  		(push (nth j tokens) (aref col (1+ j)))
  		(loop for i from (1- j) downto 0
  			  do (loop for k from (1+ i) to j
					   do (let* ((col-i (aref table i))
								 (col-k (aref table k))
								 (left (aref col-i k))
								 (right (aref col-k (+ 1 j))))
							(and left right
							  (push (list left right) (aref (aref table (1+ j)) i))
							  ;;(push (list left right) (aref (aref table (1+ j)) i))
							  ;;(push (cyk-match grammar left right) (aref (aref table (1+ j)) i))
							  ))))))))


(defun view-list (list)
  "Convenience function for viewing list items."
  (loop for x being the elements of list collect x collect "\n"))


;;	 "\n" :binarized (loop for rule being the elements of binarized collect rule collect "\n\n")
;;"binary reduce" (binarize '(RAINBOW RED ORANGE YELLOW GREEN BLUE INDIGO)) "\n"


;;(cyk-match '((A B C) (D E F) (G H I)) '(C B H D) '(E F I C))

;; '((A B) (B C D E F "a") (D X Y "z"))

(let* ((steps (cnf-steps (or plus-times-grammar)))
	   (normalized (plist-get steps :normalized))
	   (table (cky-recognize normalized '(int + int * { int + int } )))
	   (lisp-to-svg-mappings (append cky-svg-mappings lisp-to-svg-mappings)))
   ;;(view-plist steps)
   ;;normalized
  ;; (view-list table)
										;normalized
  (let* ((window (get-buffer-window lisp-play--output-buffer))
		 (width (float (window-pixel-width)))
		 (extra-height (cdr (window-text-pixel-size window nil nil nil nil t)))
		 (height (- (float (window-pixel-height window)) extra-height)))
	(svg
	 (lisp-to-xml
	  `(svg (:width ,(* width 1) :height ,(* height 1))
			,@(lisp-to-svg
			  table
			  (list (list :width width :height height))
			  )
			))))
  )

;; eval in global context to support SVG
;; (setq lisp-play-imperative-mode t)
