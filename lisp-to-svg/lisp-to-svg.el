(require 'lisp-to-xml)

(defun string-to-svg (s &optional contexts)
  "Return a ‘lisp SVG’ object containing text elements to
represent the string S."
  `(text (:x 0 :y 0 :dy "1em" :font-size 64 :fill "white" :stroke "none") ,s))

(defun symbol-to-svg (symbol &optional contexts)
  (string-to-svg (symbol-name symbol) contexts))

;; THis isn't necessarily the best way to view cons cells.
(defun list-to-svg (list &optional contexts)
  (vector-to-svg `[,@list] contexts))

(defun number-to-svg (number &optional contexts)
  (string-to-svg number contexts))

;; This looks like a tree map, except that there are no proportions.

(defun vector-to-svg (v &optional contexts)
  "Return a ‘lisp SVG’ object containing a graphical context for
  each element in the vector V."
  (let ((parent-context (car contexts)))
	(destructuring-bind
		(&whole w
				&key type (width 100.0) (height 100.0) (direction)
				&allow-other-keys)
		parent-context
	  (let* ((count (length v))
			 (rotate (eq direction 'down))
			 (downwards (eq direction 'down))
			 (item-width (if downwards width (/ width (float count))))
			 (item-height (if downwards  (/ height (float count)) height))
			 (new-context (list :type 'vector :length count)))
		`(svg (
			   :overflow "visible"		; not working
			   ,@(if downwards
					 `(:width ,height :height ,width)
				   `(:width ,width :height ,height)))
			  ;; This shouldn't matter, but it does.
			  (g (:overflow "visible")
				 ,@(loop for ele being the elements of v
						 for i = 0.0 then (1+ i)
						 collect
						 `(g (:transform

							  ,(format "translate(%.3f,%.3f)"
									   (if rotate 0 (* i item-width))
									   (if rotate (* i item-height) 0)
									   ))
							 
							 ;; This is nice for reference, but shouldn't be done here.
							 (rect (:x 0 :y 0 :width "100%" :height "100%"
									   :fill "rgba(200, 100, 100, .3)"
									   ;;:stroke "yellow"
									   :stroke "rgba(255, 255, 255, .2)"
									   :stroke-width 8))

							 ,@(lisp-to-svg
								ele
								(cons (nconc (list :width item-width :height item-height
												   :direction (if (eq direction 'down) 'across 'down))
											 new-context)
									  contexts))))))))))

(setq lisp-to-svg-mappings
	  '(
		(consp list-to-svg)
		(stringp string-to-svg)
		(symbolp symbol-to-svg)
		(numberp number-to-svg)
		(vectorp vector-to-svg)
		((lambda (x) t)
		 (lambda (x &optional cs) '(circle (:cx "50%" :cy "50%" :r 130 :fill "rgba(83, 0, 0, .8)")))
		 t)
		))

(defun lisp-to-svg (object &optional contexts)
  "Return a ‘lisp SVG’ object representing a graphical projection
of OBJECT.  This function is just a dispatcher that uses
`lisp-to-svg-mappings' to select a formatter."
  (loop for (pred formatter opts) in lisp-to-svg-mappings
		when (apply pred (if opts (list object contexts) (list object)))
		return (list (funcall formatter object contexts))))

(provide 'lisp-to-svg)
