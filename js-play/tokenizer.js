/*global module*/

"use strict";

function coroutine(f) {
	return function(...args) {
		const o = f(...args);
		o.next();
		return o;
	};
}

// Tokenize a chunked string sequence (single-character).

// Chunk split by token:
// [] -> empty, ignore
// [''?, ...rest] -> initial blank means chunk started with delimiter for prior data, so yield buffer
// rest:
//   [f] -> buffer f
//   [f, ...cs, ''] -> yield buffer + f, yield *cs
//   [f, ...cs, g] -> yield buffer + f, yield *cs, and buffer g

const tokenize = coroutine(function * tokenize(token, sink) {
	
	const buffer = [];
	function flush() {
		try { return buffer.join(''); }
		finally { buffer.length = 0; }
	}
	
	try {
		while (true) {
			const chunk = yield;
			
			const [first, ...rest] = chunk.split(token);
			
			if (first === '') sink.next(flush());
			else if (first !== undefined) buffer.push(first);
			
			if (rest.length !== 0) {
				const last = rest.pop();
				
				if (buffer.length !== 0) sink.next(flush());
				if (rest.length !== 0)
					for (let word of rest)
						sink.next(word);
				
				if (last !== '') buffer.push(last);
			}
		}
	}
	finally {
		yield flush();
	}
});

module.exports = {
	coroutine,
	tokenize
};
