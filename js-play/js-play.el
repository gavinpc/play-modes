;; Javascript playground

;; TODO: kill process when input buffer is closed
;; TODO: interrupt any ongoing calculation before sending a new one
;; TODO: support switching of input buffers on same output (but how do you decide which ones?)
;; TODO: sometimes `Selecting deleted buffer` error when attempting to close source (after reloading mode?)
;;                     or at least after loading mode in another js file
;; TODO: make this a minor mode?
;; TODO: support output in another frame
;; TODO: use lexical scoping
;; TODO: buffer management breaks some things, like showing help buffer

(defvar js-play-input-buffer nil
  "The javascript buffer that's wired to the live output.")

(defvar-local js-play-use-parse-tree t
  "Whether to use (when available) the parse tree and hook from
`js2-mode' to send code to the javascript process
statement-by-statement.  Disabling this means that all code will
be sent on every update, regardless of parse errors.  This can be
used for testing JS constructs not yet supported in `js2-mode'
such as (as of this writing), `for await' loops
(see https://github.com/mooz/js2-mode/issues/412)")


;; private stuff

(defvar js-play--output-buffer nil
  "The buffer dedicated to the results of the input source.")

(defvar js-play--process nil
  "When initialized, this contains a javascript process dedicated
to a debug-watch like interface.")

(defvar js-play--previous-buffer nil)


(defconst js-play--repl-script-file
  (expand-file-name "play.js"
					(if load-file-name
						(file-name-directory load-file-name)
					  default-directory))
  "The file location of the script to be run by the javascript
process.  It's assumed to be located in the same directory as
this script.")

(defun js-play--cancel-running-script ()
  "Instruct the javascript process to clear any running scripts."
  (if (process-live-p js-play--process)
	  (interrupt-process js-play--process)))

(defun js-play--send-code-block (code)
  "Send CODE to the javascript process, creating the latter if it
is not already running."
  (unless (process-live-p js-play--process)
	(js-play--start-process))
  (if (process-live-p js-play--process)
	  (process-send-string js-play--process (concat code "\x00"))))

(defun js-play--send-code (&optional code)
  "Send the full contents of the code buffer to the javascript
process, creating the latter if it is not already running."
  (unless (process-live-p js-play--process)
	(js-play--start-process))
  (process-send-string js-play--process "\x03")
  (with-current-buffer js-play-input-buffer
	(js-play--send-code-block
	 (or code (buffer-substring-no-properties (point-min) (point-max))))))

(defun js-play--send-source ()
  "Send contents of the input buffer to the javascript engine.
This should be run whenever you've made a change to the source.
Note that this proceeds even if the buffer has parse errors."
  (js-play--show-results (buffer-string)))


(defun last-index-of (char string)
  "Return the position of the last occurrence of CHAR in STRING,
  or nil if it does not appear."
  (loop for i from (1- (length string)) downto 0
		when (= (aref string i) char)
		return i))



;; Regarding the input stream from the process:
;; 
;; - logically, the stream is divided into “payloads”.
;; 
;; - physically, the stream is divided into “chunks”.
;; 
;; - a null character signals the start of a new payload (and, implicitly, the
;;   end of the previous one.)
;; 
;; - a payload starting with a backspace character is a “message”
;; 
;; - only complete messages can be processed.
;; 
;; - a payload that is *not* a message is “raw text”.
;; 
;; - chunks of raw text can be dumped immediately to the output buffer (without
;;   waiting for the rest of the payload).
;; 
;; - a form feed character means stop anything you're in the middle of and clear
;;   the screen.  “Stop anything” means clear any buffers and stop any
;;   evaluations (if you can).

;; nil would also work
(defvar
  default-tokenizer-state
  (list
   :complete nil	 ; a list of complete payloads (strings)
   :partial nil	 ; a list of parts of an incomplete payload (following complete)
   ))


;; If the chunk contains any breaks, then the completed list will contain an
;; item comprising any first element, with any pending buffer prepended.  The
;; completed list will always contain any items from the "middle", since only
;; the first and last items can be fragments.  It will never contain the "last"
;; item, since the last item will be either blank, meaning the chunk ended with
;; a break (thus ending the foregoing payload), or did not, in which case the
;; last part is a fragment.
(defun tokenize-accept-chunk (state chunk)
  "With CHUNK as the next input, return the tokenizer state that
follows STATE.  Assumes any previously-completed payloads have
already been processed."
  (let*
	  ((parts (split-string chunk "\0"))
	   (first (first parts))
	   (rest (rest parts))
	   (last (car (last rest)))
	   (buffer (plist-get state :partial)))
	(list
	 :complete (if rest (nconc
						 (unless (equal "" first)
						   (list (concat (apply 'concat buffer) first)))
						 (butlast rest)))
	 :partial (if last
				  (unless (equal "" last) (list last))
				(append buffer (unless (equal "" first) (list first)))))))


(defun js-play--has-message-header (payload)
  "Tell whether PAYLOAD is the start of a ‘message,’ that is, a
payload that must be processed in its entirety."
  (and (> (length payload) 0) (= (aref payload 0) 8))) ; backspace \b

(defun js-play--process-raw-text (string)
  "Do side-effecting for the raw text STRING.  Dumps text to the
end of the output buffer.  A form-feed character is interpreted
as ‘clear screen.’  Note that this does not maintain a separate
‘process marker’ where output is inserted.  Instead, output is
always added to the end, while the cursor is kept at the
beginning, so that the top of the buffer remains visible."
  (let ((buffer js-play--output-buffer)
		(cls-index (last-index-of 12 string))) ; form feed x0C
	(with-current-buffer buffer
	  (save-excursion
		(when cls-index
		  (setq string (substring string (1+ cls-index)))
		  (erase-buffer))
		(goto-char (point-max))
		(insert string))
	  ;; Seems neither of these matter ATM
	  ;; (goto-char (point-min))
	  ;;(goto-char (point-max))
	  )))

(defun svg (svg)
  "Insert the given SVG as an image at point, or, if there is
  already an SVG image at point, update the SVG and advance
  point."
  (let ((point (point))
		(image (list 'image :type 'svg :data svg)))
	(if (not (eq 'svg (plist-get (cdr (get-text-property point 'display)) :type)))
		(insert-image image)
	  (put-text-property point (1+ point) 'display image)
	  (forward-char))))

(defun js-play--process-message (message)
  "Perform action appropriate for MESSAGE.  At the moment,
messages are interpreted as Emacs Lisp code and evaluated for
side-effects (i.e. their result is ignored).  Also, code is
evaluated in the context of the output buffer, regardless of the
current buffer."
  (with-current-buffer js-play--output-buffer
	(let ((debug-on-exit t))
	  (condition-case err
		  (eval (read (concat "(progn " message " )")) t)
		(error (insert (error-message-string err)))))))


(defun js-play--process-state (state)
  "Do any side-effecting appropriate for the given STATE and
return a (possibly) modified state.  Any ‘completed’ payloads
will be processed, and a partial payload may be processed as well
if it is in ‘raw’ mode (i.e. doesn't need to be complete)."
  (loop
   for payload in (plist-get state :complete)
   do (if (js-play--has-message-header payload)
		  (js-play--process-message payload)
		(js-play--process-raw-text payload)))
  (let ((partial (plist-get state :partial)))
	(if (js-play--has-message-header (first partial))
		state
	  (js-play--process-raw-text (apply 'concat partial))
	  ;; This also dumps :complete, but they're ignored anyway
	  nil)))

(defun js-play--output-filter (proc chunk)
  "Respond to a CHUNK of input from the javascript process."
  (when (buffer-live-p js-play--output-buffer)
	(setq js-play--tokenizer-state (tokenize-accept-chunk js-play--tokenizer-state chunk))
	(setq js-play--tokenizer-state (js-play--process-state js-play--tokenizer-state))))

(defun js-play--get-block-index (ast)
  "Return metadata about the top-level nodes in the given AST."
  (let ((index []))
	(js2-visit-ast
	 ast
	 '(lambda (node end-p)
		(let ((type (aref node 0)))
		  ;; Returning nil means don't visit children
		  (cond
		   ((or end-p (eq type 'cl-struct-js2-comment-node)))
		   ((eq type 'cl-struct-js2-ast-root) t)
		   ((setq index (append index `((,(aref node 2) ,(aref node 3) ,type))))
			nil)))))
	index))

(defun js-play--after-parse ()
  "Send all statements to the javascript process, one at a time."
  (unless js2-parsed-errors
	;; Signal to cancel any running script and create a new context.
	;; 
	;; TODO: not sure if this counts as a SIGINT for the purpose of interrupting running script.
	(if (process-live-p js-play--process)
		(process-send-string js-play--process "\x03"))
	;;(js-play--cancel-running-script)
	(with-current-buffer js-play-input-buffer
	  (let ((index (js-play--get-block-index js2-mode-ast)))
		(loop for (pos len) in index do
			  (js-play--send-code-block
			   (buffer-substring-no-properties pos (+ 1 pos len))))))))


;; BATCH this? see docs
(defun js-play--after-change (change-beg change-end prev-len)
  "Respond to buffer changes by triggering output update"
  (js-play--send-source))


;; window management

(defun js-play--ensure-buffer-visible (buffer)
  "Display the output window if it is not already visible."
  (unless (get-buffer-window buffer)
	(display-buffer buffer)))

(defun js-play--ensure-output-visible ()
  (js-play--ensure-buffer-visible (js-play-output-buffer)))

(defun js-play--window-config-changed ()
  "Ensure that output buffer is shown whenever input buffer is
  selected, and vice versa."
  (let ((prev js-play--previous-buffer)
		(current (current-buffer))
		(input js-play-input-buffer)
		(output (js-play-output-buffer)))
	(unless (eq current prev)
	  (setq js-play--previous-buffer (current-buffer))
	  (cond
	   ((eq current input) (js-play--ensure-buffer-visible output))
	   ((eq current output) (js-play--ensure-buffer-visible input))))))


;; process management

(defun js-play--start-process ()
  "Start (or restart) a javascript process in the dedicated buffer."
  (setq js-play--tokenizer-state nil)
  (js-play--kill-process)
  (setq js-play--process
		(let ((process-environment
			   (cons (concat
					  "NODE_PATH="
					  (file-name-directory
					   (buffer-file-name js-play-input-buffer))
					  "node_modules")
					 process-environment)))
		  (make-process
		   :name "nodejs watch"
		   :command
		   (list
			"node"
			"--expose-gc"
			js-play--repl-script-file
			;; Pass the buffer file name so we can set __dirname
			(buffer-file-name js-play-input-buffer))
		   :sentinel 'js-play--sentinel
		   :filter 'js-play--output-filter
		   :connection-type (if (eq system-type 'darwin) 'pipe) ; OSX: default type doesn't work
		   :noquery t)))
  (with-current-buffer (js-play-output-buffer)
	;; See note in `js-play--start-with-buffer'.
  	;;(make-local-variable 'kill-buffer-hook)
  	(add-hook 'kill-buffer-hook 'js-play--cleanup-output nil t))
  ;; If the process failed to start, this buffer will tell why.
  (js-play--ensure-output-visible))

(defun js-play--kill-process ()
  "Kill the running `js-play--process' if it exists."
  (if (process-live-p js-play--process)
  	  (delete-process js-play--process)))

(defun js-play--sentinel (process event)
  "DIsplay signals from the javascript process---generally
termination signals."
  (and (buffer-live-p js-play--output-buffer)
	   (not (string-prefix-p "interrupt" event))
	   (with-current-buffer js-play--output-buffer
		 (goto-char (point-min))
		 (insert (concat "SIGNAL from javascript process:\n" event "\n\n")))))


;; emacs event management

(defun js-play--cleanup-output ()
  "Kill javascript process and remove hooks used by the output
buffer."
  (js-play--kill-process)
  ;;(remove-hook 'window-configuration-change-hook 'js-play--window-config-changed)
  (remove-hook 'buffer-list-update-hook 'js-play--window-config-changed)
  (if js-play-input-buffer
	  (with-current-buffer js-play-input-buffer
		(remove-hook 'after-change-functions 'js-play--after-change t))))

(defun js-play--cleanup-everything ()
  "Remove everything associated with the watcher."
  (js-play--cleanup-output)
  (kill-buffer js-play--output-buffer))


;; the actual thing

(defun js-play--show-results (code)
  "Evaluate CODE using `js-play--process' and replace the
  contents of `js-play-output-buffer' with the results."
  ;; TODO: this is out of date.  I'm keeping it to support modes other than js2,
  ;; but for ex, the `erase-buffer' is superceded by use of control characters
  ;; from output process, which applies regardles of input mode.
  (let ((buffer (js-play-output-buffer)))
	(with-current-buffer buffer (erase-buffer))
	(js-play--send-code code)
	(js-play--ensure-output-visible)
	(with-current-buffer buffer (goto-char (point-min)))))

(defun js-play--start-with-buffer (buffer)
  "Configure BUFFER to serve as input source for a javascript
process."
  (with-current-buffer buffer
	(setq js-play-input-buffer buffer)
	
	;; Supposedly this is done when you use `add-hook' with the ‘local’ option,
	;; but omitting these is giving problems with cleanup.
	;; but see Info page on Creating Buffer-local
	;; (make-local-variable 'after-change-functions)
	;; (make-local-variable 'kill-buffer-hook)
	;; (make-local-variable 'js2-parse-finished-hook)
	
	(add-hook 'kill-buffer-hook 'js-play--cleanup-everything t t)
	;; TODO: unlike window-config-change hook, this covers switching to other
	;; window.  but this won't allow e.g. help buffer when you're on the input or
	;; output buffers.
	(add-hook 'buffer-list-update-hook 'js-play--window-config-changed)
	
	(js-play--start-process)
	
	(if (and js-play-use-parse-tree (boundp 'js2-parse-finished-hook))
		(progn 
		  (add-hook 'js2-parse-finished-hook 'js-play--after-parse nil t)
		  ;; TODO: if we're loading immediately, parse may not be complete yet
		  (js-play--after-parse))
	  (add-hook 'after-change-functions 'js-play--after-change t t)
	  (js-play--send-source))))


;; public stuff

(defun js-play-output-buffer ()
  "Return the output buffer, creating it if necessary."
  (or
   (and (buffer-live-p js-play--output-buffer)
		js-play--output-buffer)
   (setq js-play--output-buffer (get-buffer-create "*js play*"))))

(defun js-play-start ()
  "Create a 'real-time' evaluation buffer wired to the current
javascript buffer.  Resets any previously-existing one."
  (interactive)
  (if (eq (current-buffer) js-play--output-buffer)
	  (message "Can't use js-play from its output buffer!")
	(js-play--start-with-buffer (current-buffer))))

(provide 'js-play)
