/*global global, require, process, module*/

"use strict";

const vm = require("vm");
const util = require("util");
const path = require("path");
const { coroutine, tokenize } = require("./tokenizer");
const { DisposableTimers } = require("./timers");

const [, , script_filename] = process.argv;

// This handler has to be present if you want to be able to
// interrupt scripts from outside.
process.on("SIGINT", () => {});

const input = process.stdin;
const output = process.stdout;

input.setEncoding("utf8");

const RESET_CONTEXT = "\x03";
const PAYLOAD_DELIMITER = "\0";
const MESSAGE_HEADER = "\b";
const CLEAR_SCREEN = "\f"; // using form feed as clear screen

const state = {};

const new_context = (function () {
  let timers = new DisposableTimers(global);
  return function () {
    timers.__clearAll();

    const global = Object.assign(Object.create(globalThis), timers, {
      state,
      window: { test: true }, // TEMP:  deal with bogus datamodel dependency
      write: output.write.bind(output),
      cls() {
        output.write("\f");
      },
      log: write_value,
      lisp: write_message,
      __dirname: path.dirname(script_filename),
      require,
      exports: {}, // prevent crash in commonjs module
      // Should just have process launched with process.cwd in NODE_PATH
      // But how to do that in emacs?
      // require: (path, ...rest) => {
      //   const IS_RELATIVE = /$\./;
      //   const is_absolute = !IS_RELATIVE.test(path);
      //   const nm = is_absolute ? `${process.cwd()}/node_modules/${path}` : path;
      //   return require(path, ...rest);
      // },
      cwd: process.cwd(),
      // Suddenly `process` is coming up undefined in client 2018-05-28
      process,
      console,
      module: {},
    });
    global.global = global;

    return vm.createContext(global);
  };
})();

let script = null;
let context = new_context();

function write_payload(text) {
  output.write(PAYLOAD_DELIMITER + text);
}

function write_message(message) {
  // Ending with the extra payload delimiter forces flush even at the end of a
  // batch.
  write_payload(MESSAGE_HEADER + message + PAYLOAD_DELIMITER);
}

function write_value(...values) {
  write_payload(util.format(...values) + "\n");
}

function eval_sync(code) {
  try {
    script = new vm.Script(code);
    const result = script.runInContext(context, {
      timeout: 5000,
      breakOnSigint: true,
    });
    //output.write(util.inspect(result));
    if (result !== undefined) write_value(result);
  } catch (error) {
    write_value(error);
  } finally {
    script = null;
  }
}

// Not exactly a tokenizer.  When the "reset context" token is seen anywhere in
// the input stream, everything prior to that point is immediately superceded,
// i.e. not passed through the stream.
const contextifier = coroutine(function* (token, sink) {
  try {
    while (true) {
      const chunk = yield;
      const parts = chunk.split(token);

      if (parts.length > 1) {
        output.write(CLEAR_SCREEN + PAYLOAD_DELIMITER);
        context = new_context();
        sink.next(parts[parts.length - 1]);
      } else sink.next(chunk);
    }
  } finally {
    // crashed?
    console.log("FINALLY block of contextifier");
  }
});

const evaluator = coroutine(function* evaluator() {
  try {
    while (true) {
      const script = yield;
      eval_sync(script);
    }
  } finally {
    // probably means crash
    console.log("FINALLY block of evaluator");
  }
});

const sink = contextifier(
  RESET_CONTEXT,
  tokenize(PAYLOAD_DELIMITER, evaluator())
);

input.on("data", (chunk) => sink.next(chunk));
