/*global module*/

// A collection of timers that you can dispose of all at once.
function DisposableTimers(context) {
	const outstanding = {};
	
	const proxy = {
		__clearAll() {
			for (let type of Object.keys(outstanding))
				for (let saved of outstanding[type])
					proxy['clear' + type](saved);
		}
	};
	
	// The bookkeeping here is perhaps a bit excessive, since clearing a
	// non-existent timeout is always a no-op.
	function proxy_method(type, prefix, add_or_remove) {
		proxy[prefix + type] = function(...args) {
			const result = context[prefix + type].apply(context, args);
			outstanding[type][add_or_remove](result);
			return result;
		};
	}
	
	for (const type of ['Interval', 'Timeout', 'Immediate']) {
		outstanding[type] = new Set();
		proxy_method(type, 'set', 'add');
		proxy_method(type, 'clear', 'delete');
	}
	
	return proxy;
}

module.exports = {
	DisposableTimers
};
