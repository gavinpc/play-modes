;;; -*- lexical-binding: t -*-

;; Buffer linking

(defun link-buffers (master slave)
  "Set up hooks to enforce the invariant that the buffer SLAVE
  will always be killed when the buffer MASTER is killed."
  (with-current-buffer master
	(add-hook 'kill-buffer-hook (apply-partially 'kill-buffer slave) nil t)))

;; Is this needed, though?  When would you do this?
(defun unlink-buffers (master slave)
  "Remove hooks related to linking of buffers MASTER and SLAVE."
  )

;; TODO: how to make these only buffer-local in the mode?
(setq play-graphviz--input-buffer nil)
(setq play-graphviz--output-buffer nil)

(defun play-graphviz-output-buffer ()
  "Return the output buffer, creating it if necessary."
  (or
   (and (boundp 'play-graphviz--output-buffer)
		(buffer-live-p play-graphviz--output-buffer)
		play-graphviz--output-buffer)
   (setq play-graphviz--output-buffer (get-buffer-create "*graphviz play*"))))

;; TODO: move to svg utils package
(defun naive-set-svg-size (svg width height)
  "Return an alternate version of SVG with any explicit svg width
and height attributes replaced by the given WIDTH and HEIGHT."
  (let ((context "<svg\[^>\]*")
		(attribute "=\\(\['\"\]\\)\\(.*?\\)\\1"))
	(string-match (concat context "width" attribute) svg)
	(setq svg (replace-match (format "%d" width) t t svg 2))
	(string-match (concat context "height" attribute) svg)
	(setq svg (replace-match (format "%d" height) t t svg 2))))



;; TODO: move to svg utils package
(defun svg-to-buffer (svg buffer)
  "Use the given BUFFER to display the given SVG as an image.
Adjusts the svg's size to the buffer's window."
  (with-current-buffer buffer
	(let* ((inhibit-read-only t)
		   ;; TODO: if the output buffer is visible on multiple frames, this
		   ;; will not set the image size appropriately for each one.
		   (window (get-buffer-window buffer t))
		   (width (float (window-pixel-width window)))
		   (extra-height (cdr (window-text-pixel-size window nil nil nil nil t)))
		   (extra-height 0)
		   (height (- (float (window-pixel-height window)) extra-height)))
	  (erase-buffer)
	  (insert (naive-set-svg-size svg width height)))
	;; `image-mode' refreshes the image but echoes an annoying "Type blah to
	;; toggle" message.  Calling toggle is lighter weight once the mode is up.
	(if (eq major-mode 'image-mode)
		(image-toggle-display-image)
	  (image-mode))))

(defun play-graphviz--update ()
  "Update the output buffer with the SVG from Graphviz, or show
its error output in the message area."
  (let ((dot (buffer-string)))
	(with-temp-buffer
	  (condition-case err
		  (if (zerop (call-process-region dot nil "dot" nil t nil "-Tsvg"))
			  (svg-to-buffer (buffer-string) (play-graphviz-output-buffer))
			(message (buffer-string)))
		(error (message (error-message-string err)))))))

(defun play-graphviz--after-change (change-beg change-end prev-len)
  "Respond to buffer changes by triggering output update.  This
is only needed because this hook takes args."
  (play-graphviz--update))

(defun enable-play-graphviz-mode ()
  "Ensure that things are set up for `play-graphviz-mode'."
  ;; Assumes this is run from the input buffer.
  (make-local-variable 'play-graphviz--input-buffer)
  (make-local-variable 'play-graphviz--output-buffer)
  (setq play-graphviz--input-buffer (current-buffer))
  (play-graphviz-output-buffer)
  (link-buffers play-graphviz--input-buffer play-graphviz--output-buffer)
  (add-hook 'after-change-functions 'play-graphviz--after-change t t)
  (play-graphviz--update))

(defun disable-play-graphviz-mode ()
  "Ensure that things are torn down for `play-graphviz-mode'."
  ;; I guess you can assume this is run from the input buffer, since it's
  ;; buffer-local there
  (and (boundp 'play-graphviz--output-buffer)
	   (buffer-live-p play-graphviz--output-buffer)
	   (kill-buffer play-graphviz--output-buffer))
  (remove-hook 'after-change-functions 'play-graphviz--after-change t))

(define-minor-mode play-graphviz-mode
  "Enable `play-graphviz-mode' in the current buffer.

This will create a buffer showing the SVG output of the current
Graphviz-dot buffer."
  :lighter " play"
  (message "play graphviz mode was set %s" play-graphviz-mode)
  (if play-graphviz-mode
	  (enable-play-graphviz-mode)
	(disable-play-graphviz-mode)))

(provide 'play-graphviz)
