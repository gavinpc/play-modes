/*global require, process, module*/

'use strict';

const {coroutine, tokenize} = require('./tokenizer');


const logger = coroutine(function*(name, sink) {
	console.log(`<<START:${name}>>`);
	try {
		while (true) {
			const yielded = yield;
			write(`${name}: ${yielded}`);
			if (sink) sink.next(yielded);
		}
	} finally {
		console.log(`<<FINALLY:${name}>>`);
	}
});

const evaluator_sink = tokenize('\x00', logger("EVAL"));
const contextifier_sink =
	  tokenize('\x03', logger("CONTEXT", evaluator_sink));


contextifier_sink.next("\x03once there was a new context\x00so do something with it\x00");
contextifier_sink.next("and you can keep doing things with it\x00\x03but if you get ");
contextifier_sink.next("a new context in the middle of a chunk\x00");
contextifier_sink.next("then you never see\x00the first part of it\x00");
contextifier_sink.next("and that's that\x00");


console.log();
const wordsink = tokenize(
	'.',
	logger("\nSENTENCE", tokenize(/[^\w]/, logger(' | '))),
	{thru:true});

wordsink.next(`Now I'm going to start wit`);
wordsink.next(`h a chunk. And what I want you to`);
wordsink.next(` notice is that the sentence token`);
wordsink.next(`izer is "in front of" the word tokenizer.`);
wordsink.next(`But the words don't need to wait for the en`);
wordsink.next(`d of a sentence before being tokenized.`);


wordsink.next(`Hello.  My name is Gavin Peter Cannizzaro.  Once up`);
wordsink.next(`pon a time.  There was a little boy.  Named baby tucoo.`);
