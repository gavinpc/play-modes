
function * take(n, iterable) {
	const gen = iterable[Symbol.iterator]();
	while (n--) {
		const {done, value} = gen.next();
		if (done) return;
		yield value;
	}
}

function * constantly(x) { while(true) yield x; }

function * take(n, iterable) {
	const gen = iterable[Symbol.iterator]();
	while (n--) {
		const {done, value} = gen.next();
		if (done) return;
		yield value;
	}
}

function * reduce(prev, fn, iterable) {
	const gen = iterable[Symbol.iterator]();
	while(true) {
		let {done, value} = gen.next();
		if (done) return;
		yield prev = fn(prev, value);
	}
}

function * map(f, iterable) {
	const gen = iterable[Symbol.iterator]();
	while (true) {
		const {done, value} = gen.next();
		if (done) return;
		yield f(value);
	}
}

function * mapWith(fn, ...iterables) {
}
//const inc = n => n+1;

function * from(n = 0, by = 1) { while (true) yield n += by; }

// clear screen so you don't see result of last eval after every change
//setImmediate(function() {console.log("\f")});

//const f = x => x ** 2;

const compose = (...fns) => x =>
	  fns.reduceRight((acc, f) => f(acc), x);

let y = 5;
setInterval(() => y++, 500) && undefined;

//const f = x => Math.round(Math.log(x ** 2));
//const f = compose(Math.round, Math.sin, Math.exp.bind(y));
//const f = compose(Math.round, Math.sin);
const times = n => x => n * x;
// const f = compose(Math.round, times(10), Math.sin, times(10));
const f = x => x * 3;

let x = 0;

if (!state.x)
	state.x = 0;

var do_stuff = function do_stuff() {
	state.x++;
	x++;
	console.log("\f");
	for (let i = 0; i < 10; i++) {
		const items = take(20, map(f, from(state.x+i)));
		//const items = take(20, map(f, from(x+1*i)));
		console.log('\0' + [...items].join(''));
	}
}

//do_stuff();
//setInterval(do_stuff, 1000);

setInterval(do_stuff, 100) && undefined;

/*
//Object.entries({a:3, b: 4, c:10, e: "bass line"});

//

//state.instr

;

if (!state.z)
	state.z = 0;

a = [...take(
	10,
	map(
		x => [...take(Math.round(x), constantly(state.z))],
		from(0)))
];

const plus = (a, b) => a+b;

const sum = list => list.reduce(plus, 0);


const all = [].concat(...a);

`total = ${sum(all)}`

//[...take(10, from(0))];
*/
