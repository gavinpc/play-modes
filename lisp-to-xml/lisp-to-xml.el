;;; lisp-to-xml.el --- serialize lisp objects as XML  -*- lexical-binding:t -*-

(defvar xml-content-encode-map
  '((?& . "&amp;")
	(?< . "&lt;")
	(?> . "&gt;")))

(defvar xml-attribute-encode-map
  (cons '(?\" . "&quot;") xml-content-encode-map))

(defun write-xml (o out parents depth)
  "Writes O as XML to OUT, assuming that lists have a plist as
their second element (for representing attributes).  Skips basic
cycles (elements pointing to ancestor), and compound values for
attributes."
  (if (not (listp o))
	  ;; TODO: good way to share this with below?
	  (princ o (lambda (charcode)
				 (princ 
				  (or (assoc charcode xml-content-encode-map)
					  (char-to-string charcode))
				  out)))

	(unless (member o parents)
	  (let ((parents-and-self (cons o parents))
			(attributes (second o)))

		(dotimes (x depth) (princ "\t" out))
		(princ "<" out)
		(princ (car o) out)
		
		;; Doesn't work for vectors :(
		(loop for x on attributes by 'cddr do
			  (let ((key (first x))
					(value (second x)))

				(when (and value (not (listp value)))
				  (princ " " out)
				  (princ (substring (symbol-name key) 1) out)
				  (princ "=\"" out)
				  (princ value  (lambda (charcode)
								  (princ 
								   (or (assoc charcode xml-attribute-encode-map)
									   (char-to-string charcode))
								   out)))
				  (princ "\"" out))))
		
		(if (not (cddr o))
			(princ " />\n" out)			; self-closing tag
		  (princ ">\n" out)

		  (loop for e in (cddr o)  do
				(write-xml e out parents-and-self (+ 1 depth)))

		  (dotimes (_ depth) (princ "\t" out))
		  (princ "</" out)
		  (princ (car o) out)
		  (princ ">\n" out)
		  )))))

(defun lisp-to-xml (object)
  "Return the lisp OBJECT serialized as an XML string."
  (with-temp-buffer
	(write-xml object (current-buffer) () 0)
	(buffer-string)))

(provide 'lisp-to-xml)
